import sys
import numpy as np
from collections import OrderedDict
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import mplhep as hep
plt.style.use(hep.style.ROOT)
import sympy

from dhi.datacard_tools import read_datacard_structured, manipulate_datacard
from dhi.models.hh_model import VBFSample, VBFFormula, GGFSample, GGFFormula

if __name__ == '__main__':

    vbf_samples = OrderedDict([
        ((1.0, 1.0, 1.0), VBFSample(CV=1.0, C2V=1.0, kl=1.0, xs=0.0017260, label="qqHH_CV_1_C2V_1_kl_1")),
        ((1.0, 1.0, 0.0), VBFSample(CV=1.0, C2V=1.0, kl=0.0, xs=0.0046089, label="qqHH_CV_1_C2V_1_kl_0")),
        ((1.0, 1.0, 2.0), VBFSample(CV=1.0, C2V=1.0, kl=2.0, xs=0.0014228, label="qqHH_CV_1_C2V_1_kl_2")),
        ((1.0, 0.0, 1.0), VBFSample(CV=1.0, C2V=0.0, kl=1.0, xs=0.0270800, label="qqHH_CV_1_C2V_0_kl_1")),
        ((1.0, 2.0, 1.0), VBFSample(CV=1.0, C2V=2.0, kl=1.0, xs=0.0142178, label="qqHH_CV_1_C2V_2_kl_1")),
        ((0.5, 1.0, 1.0), VBFSample(CV=0.5, C2V=1.0, kl=1.0, xs=0.0108237, label="qqHH_CV_0p5_C2V_1_kl_1")),
        ((1.5, 1.0, 1.0), VBFSample(CV=1.5, C2V=1.0, kl=1.0, xs=0.0660185, label="qqHH_CV_1p5_C2V_1_kl_1")),
    ])
    ggf_samples = OrderedDict([
        ((0.0,  1.0), GGFSample(kl=0.0,  kt=1.0, xs=0.069725, label="ggHH_kl_0_kt_1")),
        ((1.0,  1.0), GGFSample(kl=1.0,  kt=1.0, xs=0.031047, label="ggHH_kl_1_kt_1")),
        ((2.45, 1.0), GGFSample(kl=2.45, kt=1.0, xs=0.013124, label="ggHH_kl_2p45_kt_1")),
        ((5.0,  1.0), GGFSample(kl=5.0,  kt=1.0, xs=0.091172, label="ggHH_kl_5_kt_1")),
    ])

    # skip_ggf = [(0, 1)] # for model_default
    # skip_vbf = [(0.5, 1, 1)] # for model_default
    # skip_vbf = [(1, 2, 1)] # for model_no_vbf_C2V2
    skip_ggf = None
    skip_vbf = None
    vbf_keys = [key for key in vbf_samples if skip_vbf is None or key not in skip_vbf]
    ggf_keys = [key for key in ggf_samples if skip_ggf is None or key not in skip_ggf]

    vbf_formula = VBFFormula([vbf_samples[key] for key in vbf_keys])
    ggf_formula = GGFFormula([ggf_samples[key] for key in ggf_keys])

    # M_inv = vbf_formula.M.pinv()
    symbol_names_vbf = ["C2V", "CV", "kl"] + list(map("xs{}".format, range(vbf_formula.n_samples)))
    xsec_func_vbf = sympy.lambdify(sympy.symbols(symbol_names_vbf), vbf_formula.sigma, 'numpy')

    symbol_names_ggf = ["kl", "kt"] + list(map("xs{}".format, range(ggf_formula.n_samples)))
    xsec_func_ggf = sympy.lambdify(sympy.symbols(symbol_names_ggf), ggf_formula.sigma, 'numpy')

    datacard = '$DHI_DATACARDS_RUN2/bbbb_boosted_vbf/v6/datacard.txt'
    target_datacard = 'datacard.txt'

    all_vbf_labels = [sample.label for sample in vbf_formula.samples] # get sample label
    all_vbf_names = [sample.label+"_hbbhbb" for sample in vbf_formula.samples] # get sample names in same order as formula
    all_ggf_labels = [sample.label for sample in ggf_formula.samples] # get sample label
    all_ggf_names = [sample.label+"_hbbhbb" for sample in ggf_formula.samples] # get sample names in same order as formula

    content = read_datacard_structured(datacard)
    all_bin_names = [p["name"] for p in content["bins"]]

    print('full vbf formula:', vbf_formula.sigma[0].subs(zip(list(map("xs{}".format, range(vbf_formula.n_samples))), all_vbf_labels)))
    print('full ggf formula:', ggf_formula.sigma[0].subs(zip(list(map("xs{}".format, range(ggf_formula.n_samples))), all_ggf_labels)))

    zero_bin_names = [bin_name for bin_name in all_bin_names if content['observations'][bin_name]==0]
    shapes = OrderedDict()
    errors = OrderedDict()
    for vbf_name in all_vbf_names:
        shapes[vbf_name] = np.zeros(len(all_bin_names))
        errors[vbf_name] = np.zeros(len(all_bin_names))
    for ggf_name in all_ggf_names:
        shapes[ggf_name] = np.zeros(len(all_bin_names))
        errors[ggf_name] = np.zeros(len(all_bin_names))

    for bin_name, rates in content["rates"].items():
        bin_index = all_bin_names.index(bin_name)
        for vbf_name in all_vbf_names:
            if vbf_name in rates:
                shapes[vbf_name][bin_index] = rates[vbf_name]
        for ggf_name in all_ggf_names:
            if ggf_name in rates:
                shapes[ggf_name][bin_index] = rates[ggf_name]

    for parameter in content["parameters"]:
        if "prop_CMS_bbbb_boosted_vbf" in parameter["name"]:
            for bin_name in all_bin_names:
                bin_index = all_bin_names.index(bin_name)
                for vbf_name in all_vbf_names:
                    if parameter["spec"][bin_name][vbf_name] != "-":
                        errors[vbf_name][bin_index] = parameter["spec"][bin_name][vbf_name]
                for ggf_name in all_ggf_names:
                    if parameter["spec"][bin_name][ggf_name] != "-":
                        errors[ggf_name][bin_index] = parameter["spec"][bin_name][ggf_name]
    print(shapes)
    print(errors)
    #np.savez('VBF/shapes', **shapes)
    #np.savez('VBF/errors', **errors)
    #sys.exit()

    def xsec_shape_vbf(C2V=1, CV=1, kl=1):
        xsec = xsec_func_vbf(C2V, CV, kl,
                             *(shapes[vbf_name] for vbf_name in all_vbf_names))[0][0]
        return xsec

    def xsec_shape_ggf(kl=1, kt=1):
        xsec = xsec_func_ggf(kl, kt,
                             *(shapes[ggf_name] for ggf_name in all_ggf_names))[0][0]
        return xsec

    plot_dists = False
    if plot_dists:
        for vbf_name in all_vbf_names:
            plt.figure()
            hep.histplot(shapes[vbf_name], range(0,len(all_bin_names)+1))
            plt.legend(title=vbf_name)
            plt.savefig('VBF/{}.pdf'.format(vbf_name))
            plt.savefig('VBF/{}.png'.format(vbf_name))
            plt.close()
        for ggf_name in all_ggf_names:
            plt.figure()
            hep.histplot(shapes[vbf_name], range(0,len(all_bin_names)+1))
            plt.legend(title=vbf_name)
            plt.savefig('VBF/{}.pdf'.format(ggf_name))
            plt.savefig('VBF/{}.png'.format(ggf_name))
            plt.close()

    C2V_vals = np.linspace(0, 2, num=11)
    kl_vals = np.linspace(-11, 15, num=11)
    neg_bin_2D_vbf = np.zeros((len(C2V_vals), len(kl_vals)))
    neg_zero_bin_2D_vbf = np.zeros((len(C2V_vals), len(kl_vals)))
    neg_bin_2D_ggf = np.zeros((len(C2V_vals), len(kl_vals)))
    neg_zero_bin_2D_ggf = np.zeros((len(C2V_vals), len(kl_vals)))
    neg_bin_2D_sum = np.zeros((len(C2V_vals), len(kl_vals)))
    neg_zero_bin_2D_sum = np.zeros((len(C2V_vals), len(kl_vals)))
    to_mask_vbf = np.empty((len(C2V_vals), len(kl_vals)), dtype='object')
    to_mask_ggf = np.empty((len(C2V_vals), len(kl_vals)), dtype='object')
    to_mask_string = []
    all_neg_bins_vbf = []
    all_neg_zero_bins_vbf = []
    all_neg_bins_ggf = []
    all_neg_zero_bins_ggf = []
    all_neg_bins_sum = []
    all_neg_zero_bins_sum = []
    X, Y = np.meshgrid(kl_vals, C2V_vals)
    for i, C2V in enumerate(C2V_vals):
        for j, kl in enumerate(kl_vals):
            # print("formula for C2V=%s, kl=%s"%(C2V, kl))
            # print(vbf_formula.sigma[0].subs(zip(["CV", "C2V", "kl"]+list(map("xs{}".format, range(vbf_formula.n_samples))), [1, C2V, kl]+all_vbf_labels)))
            shape_vbf = xsec_shape_vbf(C2V=C2V,kl=kl)
            neg_bins_vbf = shape_vbf < 0
            neg_bin_names_vbf = list(np.array(all_bin_names)[neg_bins_vbf])
            neg_zero_bin_names_vbf = list(set(neg_bin_names_vbf) & set(zero_bin_names))
            all_neg_bins_vbf = list(set(all_neg_bins_vbf) | set(neg_bin_names_vbf))
            all_neg_zero_bins_vbf = list(set(all_neg_zero_bins_vbf) | set(neg_zero_bin_names_vbf))
            neg_bin_2D_vbf[i, j] = len(neg_bin_names_vbf)
            neg_zero_bin_2D_vbf[i, j] = len(neg_zero_bin_names_vbf)

            shape_ggf = xsec_shape_ggf(kl=kl)
            neg_bins_ggf = shape_ggf < 0
            neg_bin_names_ggf = list(np.array(all_bin_names)[neg_bins_ggf])
            neg_zero_bin_names_ggf = list(set(neg_bin_names_ggf) & set(zero_bin_names))
            all_neg_bins_ggf = list(set(all_neg_bins_ggf) | set(neg_bin_names_ggf))
            all_neg_zero_bins_ggf = list(set(all_neg_zero_bins_ggf) | set(neg_zero_bin_names_ggf))
            neg_bin_2D_ggf[i, j] = len(neg_bin_names_ggf)
            neg_zero_bin_2D_ggf[i, j] = len(neg_zero_bin_names_ggf)

            # sum of shapes neg.
            neg_bins_sum = (shape_ggf+shape_vbf) < 0
            neg_bin_names_sum = list(np.array(all_bin_names)[neg_bins_sum])
            neg_zero_bin_names_sum = list(set(neg_bin_names_sum) & set(zero_bin_names))
            all_neg_bins_sum = list(set(all_neg_bins_sum) | set(neg_bin_names_sum))
            all_neg_zero_bins_sum = list(set(all_neg_zero_bins_sum) | set(neg_zero_bin_names_sum))
            neg_bin_2D_sum[i, j] = len(neg_bin_names_sum)
            neg_zero_bin_2D_sum[i, j] = len(neg_zero_bin_names_sum)

            to_mask_vbf[i, j] = neg_zero_bin_names_vbf
            to_mask_ggf[i, j] = neg_zero_bin_names_ggf
            tmp = ["'{},qqHH_*'".format(ch) for ch in neg_zero_bin_names_vbf] + ["'{},ggHH_*'".format(ch) for ch in neg_zero_bin_names_ggf]
            to_mask_string.append(" ".join(tmp))
            if plot_dists:
                plt.figure()
                hep.histplot(shape_vbf, range(0,len(all_bin_names)+1))
                vbf_name = 'qqHH_CV_{}_C2V_{}_kl_{}_hbbhbb'.format(1, C2V, kl).replace('.','p')
                plt.legend(title=vbf_name+'\n(interp.)')
                plt.savefig('VBF/{}.pdf'.format(vbf_name))
                plt.savefig('VBF/{}.png'.format(vbf_name))
                plt.close()

                plt.figure()
                hep.histplot(shape_ggf, range(0,len(all_bin_names)+1))
                ggf_name = 'ggHH_kl_{}_kt_{}_hbbhbb'.format(kl, 1).replace('.','p')
                plt.legend(title=ggf_name+'\n(interp.)')
                plt.savefig('VBF/{}.pdf'.format(ggf_name))
                plt.savefig('VBF/{}.png'.format(ggf_name))
                plt.close()

                plt.figure()
                hep.histplot(shape_vbf+shape_ggf, range(0,len(all_bin_names)+1))
                hh_name = 'HH_CV_{}_C2V_{}_kl_{}_kt{}_hbbhbb'.format(1, C2V, kl, 1).replace('.','p')
                plt.legend(title=hh_name+'\n(interp.)')
                plt.savefig('VBF/{}.pdf'.format(hh_name))
                plt.savefig('VBF/{}.png'.format(hh_name))
                plt.close()

    plt.pcolor(X, Y, neg_zero_bin_2D_vbf)
    plt.colorbar()
    plt.savefig('VBF/{}.png'.format('neg_zero_bins_vbf'))
    plt.savefig('VBF/{}.pdf'.format('neg_zero_bins_vbf'))
    plt.close()

    plt.pcolor(X, Y, neg_zero_bin_2D_ggf)
    plt.colorbar()
    plt.savefig('VBF/{}.png'.format('neg_zero_bins_ggf'))
    plt.savefig('VBF/{}.pdf'.format('neg_zero_bins_ggf'))
    plt.close()

    plt.pcolor(X, Y, neg_zero_bin_2D_sum)
    plt.colorbar()
    plt.savefig('VBF/{}.png'.format('neg_zero_bins_sum'))
    plt.savefig('VBF/{}.pdf'.format('neg_zero_bins_sum'))
    plt.close()

    print(kl_vals)
    print(C2V_vals)
    tmp = ["'{},qqHH_*'".format(ch) for ch in all_neg_zero_bins_vbf] + ["'{},ggHH_*'".format(ch) for ch in all_neg_zero_bins_ggf]
    all_to_mask_string = " ".join(tmp)
    print('all_to_mask_string:') 
    print(all_to_mask_string)
    print('to_mask_string_unique:')
    print(list(set(to_mask_string)))

